var map = L.map('main_map').setView([-25.3737989, -57.4139595], 8);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([-25.3, -57.4]).addTo(map);
//L.marker([-25.379732374406636, -57.416994525154784]).addTo(map);
//L.marker([-25.24179523680397, -57.514577400562445]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);            
        });
    }
})